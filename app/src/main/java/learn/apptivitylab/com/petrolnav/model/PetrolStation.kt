package learn.apptivitylab.com.petrolnav.model

/**
 * Created by apptivitylab on 12/01/2018.
 */
class PetrolStation {
    private var id : Int? = null
    private var name : String? = null
    private var address : String? = null
    private var operatingHours : String? = null
    private var petrol: List<Petrol>? = null
}